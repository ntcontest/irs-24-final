import cv2
import time
import socket
from nto.final import Task


## Здесь должно работать ваше решение
def solve():
    ## Пример отправки сообщения на робота по протоколу udp
    UDP_IP = '192.168.2.137'
    UDP_PORT = 5005
    MESSAGE = b'Hello, World!'

    print("UDP target IP: %s" % UDP_IP)
    print("UDP target port: %s" % UDP_PORT)
    print("message: %s" % MESSAGE)

    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # UDP
    sock.sendto(MESSAGE, (UDP_IP, UDP_PORT))
    
    ## Запуск задания и таймера (внутри задания)
    task = Task()
    task.start()
    print(task.getTask())
    ## Загружаем изображение из задачи
    # сцена отправляется с определенной частотй
    # для этого смотри в документацию к задаче
    sceneImg = task.getTaskScene()
    mapImage = task.getTaskMap()
    cv2.imshow('some_1', sceneImg[0])
    cv2.imshow('some_2', sceneImg[1])
    cv2.waitKey(0)

    robotsSize = task.robotsSize()
    print('Number of robots:', robotsSize)
    print("Robot 0")
    state = task.getRobotState(0)
    # print("Left Motor angle:", state.leftMotorAngle)
    # print("Right Motor angle:", state.rightMotorAngle)
    # print("Left Motor speed:", state.leftMotorSpeed)
    # print("Right Motor speed:", state.rightMotorSpeed)


    # while (run):
    for i in range(10):

        v = [10, 5]
        task.setMotorVoltage(0, v)

        sceneImg = task.getTaskScene()
        #cv2.imshow('Scene', sceneImg)
        #cv2.waitKey(20) # мс
        print("Turn ", i)
        time.sleep(1)

    task.stop()

if __name__ == '__main__':
    solve()
